Source: golang-github-pkg-diff
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Faustin Lammler <faustin@fala.red>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-pkg-diff
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-pkg-diff.git
Homepage: https://github.com/pkg/diff
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/pkg/diff

Package: golang-github-pkg-diff-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: go library used to create, modify, and print diffs
 The top level package, diff, contains convenience functions for the most
 common uses.
 .
 The sub-packages provide very fine-grained control over every aspect:
   *  myers creates diffs using the Myers diff algorithm.
   *  edit contains the core diff data types.
   *  ctxt provides tools to reduce the amount of context in a diff.
   *  write provides routines to write diffs in standard formats.
